
# GKE RBAC Module

A Terraform module for creating GKE user and providing him with K8s custom role

- [GCP Kubernetes Engine roles](https://cloud.google.com/iam/docs/understanding-roles#kubernetes-engine-roles)
- [Using RBAC Authorization](https://kubernetes.io/docs/reference/access-authn-authz/rbac/#referring-to-resources)


## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_google"></a> [google](#requirement\_google) | ~> 4.44.1 |
| <a name="requirement_kubernetes"></a> [kubernetes](#requirement\_kubernetes) | ~> 2.7.1 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_gcp_email"></a> [gcp\_email](#input\_gcp\_email) | Email of GCP user. Will be invited into the project if has no access yet | `string` | n/a | yes |
| <a name="input_gcp_project"></a> [gcp\_project](#input\_gcp\_project) | The current GCP project ID. | `string` | n/a | yes |
| <a name="input_gcp_role"></a> [gcp\_role](#input\_gcp\_role) | roles for GKE Identity and Access Management | `string` | `"roles/container.clusterViewer"` | no |
| <a name="input_gcp_role_only"></a> [gcp\_role\_only](#input\_gcp\_role\_only) | indicates whether K8s clusterRole and roleBinding should be created or not | `bool` | `false` | no |
| <a name="input_k8s_namespace"></a> [k8s\_namespace](#input\_k8s\_namespace) | Kubernetes namespace to which bind the clusterRole to | `string` | `"default"` | no |
| <a name="input_k8s_role_api_groups"></a> [k8s\_role\_api\_groups](#input\_k8s\_role\_api\_groups) | which API groups should be targeted by the role | `list(string)` | <pre>[<br>  ""<br>]</pre> | no |
| <a name="input_k8s_role_name"></a> [k8s\_role\_name](#input\_k8s\_role\_name) | Name of the K8s clusterRole | `string` | `""` | no |
| <a name="input_k8s_role_resources"></a> [k8s\_role\_resources](#input\_k8s\_role\_resources) | defines to which resources has the role access to | `list(string)` | <pre>[<br>  "pods"<br>]</pre> | no |
| <a name="input_k8s_role_verbs"></a> [k8s\_role\_verbs](#input\_k8s\_role\_verbs) | represent actions that can be done to selected resources | `list(string)` | <pre>[<br>  "get",<br>  "list"<br>]</pre> | no |

## Examples

```
module "example" {
  source = "./modules/rbac"

  # REQUIRED
  gcp_email   = "user1@gmail.com"
  gcp_project = "PROJECT_ID"

  # OPTIONAL
  gcp_role            = "roles/container.clusterViewer"
  gcp_role_only       = false
  k8s_namespace       = "default"
  k8s_role_name       = "editor"
  k8s_role_api_groups = [""]
  k8s_role_resources  = ["pods", "pods/portforward", "pods/logs", "pods/exec"]
  k8s_role_verbs      = ["patch", "update", "watch"]

}
```

```
module "for_each_example" {
  source = "./modules/rbac"

  for_each = {
    "user1" = {
      gcp_email          = "user1@gmail.com"
      gcp_role           = "roles/container.viewer"
      k8s_namespace      = "default"
    }
    "user2" = {
      gcp_email          = "user2@gmail.com"
      k8s_namespace      = "networking"
      k8s_role_resources = ["pods", "service"]
      k8s_role_verbs     = ["get", "list", "create"]
    }
    "user3" = {
      gcp_email          = "user3@gmail.com"
      gcp_role           = "roles/container.clusterViewer"
      gcp_role_only      = true
      k8s_role_name      = "cluster-viewer"
    }
  }

  gcp_email = each.value.gcp_email

  k8s_namespace       = lookup(each.value, "k8s_namespace", "")
  gcp_project         = lookup(each.value, "gcp_project", "PROJECT_ID")
  gcp_role            = lookup(each.value, "gcp_role", "roles/container.clusterViewer")
  k8s_role_name       = lookup(each.value, "k8s_role_name", "ROLE_NAME:${each.key}")
  k8s_role_api_groups = lookup(each.value, "k8s_role_api_groups", [""])
  k8s_role_resources  = lookup(each.value, "k8s_role_resources", ["pods", "pods/portforward", "pods/logs", "pods/exec"])
  k8s_role_verbs      = lookup(each.value, "k8s_role_verbs", ["patch", "update", "watch"])
  gcp_role_only       = lookup(each.value, "gcp_role_only", false)

}
```
## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | ~> 4.44.1 |
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | ~> 2.7.1 |

## Resources

| Name | Type |
|------|------|
| [google_project_iam_binding.main](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/google_project_iam#google_project_iam_binding) | resource |
| [kubernetes_cluster_role.main](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/cluster_role) | resource |
| [kubernetes_role_binding.main](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/role_binding) | resource |


## Outputs

| Name | Description |
|------|-------------|
| <a name="output_k8s_role_name"></a> [k8s\_role\_name](#output\_k8s\_role\_name) | Name of K8s role created by the module |