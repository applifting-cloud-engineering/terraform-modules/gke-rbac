terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.18"
    }
    google = {
      source  = "hashicorp/google"
      version = "~> 4.71"
    }
  }
}