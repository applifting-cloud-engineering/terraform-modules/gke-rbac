output "k8s_role_name" {
  description = "Name of K8s role created by the module"
  value       = var.k8s_role_name == "" ? local.gcp_username : var.k8s_role_name
}
