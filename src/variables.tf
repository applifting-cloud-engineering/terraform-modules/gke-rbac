variable "gcp_email" {
  description = "Email of GCP user. Will be invited into the project if has no access yet"
  type        = string
}

variable "gcp_project" {
  description = "The current GCP project ID."
  type        = string
}

variable "gcp_role" {
  description = "roles for GKE Identity and Access Management"
  type        = string
  default     = "roles/container.clusterViewer"
}

variable "gcp_role_only" {
  description = "indicates whether K8s clusterRole and roleBinding should be created or not"
  type        = bool
  default     = false
}


variable "k8s_namespace" {
  description = "Kubernetes namespace to which bind the clusterRole to"
  type        = string
  default     = "default"
}

variable "k8s_role_name" {
  description = "Name of the K8s role"
  type        = string
  default     = ""
}

variable "k8s_role_api_groups" {
  description = "which API groups should be targeted by the role (empty string indicates the core API group)"
  type        = list(string)
  default     = [""]
}

variable "k8s_role_resources" {
  description = "defines to which resources has the role access to"
  type        = list(string)
  default     = ["pods"]
}

variable "k8s_role_verbs" {
  description = "represent actions that can be done to selected resources"
  type        = list(string)
  default     = ["get", "list"]
}
