locals {
  gcp_username = replace(regex("(.*)@(.*)", var.gcp_email).0, ".", "-")
}

resource "google_project_iam_member" "main" {

  project = var.gcp_project
  role    = var.gcp_role
  member  = "user:${var.gcp_email}"
}

resource "kubernetes_cluster_role" "main" {

  count = var.gcp_role_only == true ? 0 : 1

  metadata {
    name = var.k8s_role_name == "" ? local.gcp_username : var.k8s_role_name
  }
  rule {
    api_groups = var.k8s_role_api_groups
    resources  = var.k8s_role_resources
    verbs      = var.k8s_role_verbs
  }
}

resource "kubernetes_role_binding" "main" {

  count = var.gcp_role_only == true ? 0 : 1

  metadata {
    name      = var.k8s_role_name == "" ? local.gcp_username : var.k8s_role_name
    namespace = var.k8s_namespace
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = var.k8s_role_name == "" ? local.gcp_username : var.k8s_role_name
  }

  subject {
    api_group = "rbac.authorization.k8s.io"
    kind      = "User"
    name      = var.gcp_email
    namespace = var.k8s_namespace
  }
}
